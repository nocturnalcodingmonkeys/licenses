# README #

This is a Xojo class for Licenses.  This makes it easy to display the various Open Source (OSI) or Commercial licenses.


### How do I get set up? ###

* clone the repo
* copy the Licenses.xojo_binary_code file and drop it on the Xojo IDE into your project.

### Contribution guidelines ###

* if you want to contribute, please fork the repo, update it, then submit for merge.

### Who do I talk to? ###

* if there is any issues, open a ticket with the repo on bitbucket.org.
* otherwise, email support at nocturnalcodingmonkeys dot com

### there is an a simple example

I  have added a simple example using the class.
